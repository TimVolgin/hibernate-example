package ru.example.hibernate.relations.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;
import ru.example.hibernate.relations.entity.ParsedQR;

import javax.persistence.LockModeType;
import javax.persistence.QueryHint;
import java.util.List;

public interface ParsedQRRepository extends JpaRepository<ParsedQR, Long> {
    @Lock(LockModeType.PESSIMISTIC_READ)
    @QueryHints(@QueryHint(name = org.hibernate.annotations.QueryHints.CACHEABLE, value = "true"))
    @Query("select c from ParsedQR c where c.id=:id")
    ParsedQR find(@Param("id") Long id);

    @EntityGraph(attributePaths = "positions")
    List<ParsedQR> findAllByClientId(Long id, Pageable pageable);


    @Query("select r from ParsedQR r where r.client.id=:id")
    List<ParsedQR> findAllByClient(@Param("id") Long id, Pageable pageable);
}
