package ru.example.hibernate.relations.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.example.hibernate.relations.entity.Position;

import java.util.List;

public interface PositionRepository extends JpaRepository<Position, Long> {
    List<Position> findAllByParsedQRIdIn(Iterable<Long> ids);
}
