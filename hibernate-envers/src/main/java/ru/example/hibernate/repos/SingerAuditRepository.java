package ru.example.hibernate.repos;

import ru.example.hibernate.entities.Singet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SingerAuditRepository extends JpaRepository<Singet, Long> {
}
