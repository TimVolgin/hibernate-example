package ru.example.hibernate.springdatajpa;

import java.util.List;

import org.springframework.stereotype.Repository;
import ru.example.hibernate.springdatajpa.entities.Singer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SingerRepository extends JpaRepository<Singer, Long> {
    List<Singer> findByFirstName(String firstName);
    List<Singer> findByFirstNameAndLastName(String firstName, String lastName);
}
