package ru.example.hibernate.relations.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.example.hibernate.relations.entity.ParsedQR;
import ru.example.hibernate.relations.repository.ParsedQRRepository;

@Service
public class ParsedQRService {
    private final ParsedQRRepository parsedQRRepository;
    private final OtherService otherService;

    @Autowired
    public ParsedQRService(ParsedQRRepository parsedQRRepository, OtherService otherService) {
        this.parsedQRRepository = parsedQRRepository;
        this.otherService = otherService;
    }

    @Transactional
    public void changeData() {
       ParsedQR parsedQR = parsedQRRepository.find(1L);
       parsedQR.setData(1234L);
       parsedQRRepository.save(parsedQR);
    }


    @Transactional
    public void changeDataRollback() {
        ParsedQR parsedQR = parsedQRRepository.findById(1L).get();
        parsedQR.setData(1234L);
        parsedQR = parsedQRRepository.findById(1L).get();
        try {
            otherService.doSomething();
        } catch (Exception e) {

        }
        parsedQRRepository.save(parsedQR);
        System.out.println("bla bla");
    }

}
